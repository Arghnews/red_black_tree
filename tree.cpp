#include <termcolor/termcolor.hpp> // Unused
#include <cassert>
#include <cmath>
#include <deque>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <numeric>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

/*
 * TODO:
 * write in order traversal iterator
 * write breadth first iterator
 * write observer_ptr - use in place of parent raw ptr, make implicitly
 *  creatable from raw pointers
 * write streambuf efficient to find max length
 * make noexcept all over, move/copy cons
 * perfect forwarding insertion/emplace etc.
 * find out way to add abstraction layer for different backends - think this is like impossible tbh
 * move utility functions into Node, typedefs for easier reading
 * Change direction enum to enum class if cleaner
 * Pissing me the fuck off. Can't think of a clean way to avoid duplicating
 * code for ie. leftmost/rightmost child whilst being clean.
 * Member variables (ie. left, right) as arguments to function?
 * Some SFINAE bollocks with compile time type to get?
 * Does class Direction invoke jmp at runtime? Template style get otherwise faster?
 * How to encapsulate iterator properly? Friend?
 * Would me cool to be able to reuse iterator for pair type or for just binary tree type
 * Add constexpr fucking everywhere (and noexcept where appropriate)
 * Add more const to parameters
 * Add node_type and associated crap
 * Add allocator
 * Init list constructor
 * Look into storing red/blackness in the lowest bit of the pointer addresses?
 * Micro optim but pretty cool
 * Really need additional abstraction layer, one of nodes for me, another the
 * public one just with the key/value type etc. iterator
 *
 * Thoughts -
 * After some thinking - breadth first iterator traversal is impossible to
 * implement efficiently as a lightweight iterator without left/right links
 * Last row is always 2^d-1 sized pointers, which is half 2^d (depth starting
 * at 0/off by one error is annoying but whatever) so linear memory
 * Can do coroutine style thing, seems best
 */

using namespace std::string_literals;

// For my own sanity. Yes it's technically UB.
namespace std
{
  template <typename A, typename B>
  constexpr std::ostream& operator<< (std::ostream& os, const std::pair<A, B>& p)
  {
    return os << "[" << p.first << ", " << p.second << "]";
  }
}

enum class Color : std::uint8_t
{
	BLACK,
	RED,
};

template <typename T>
struct Node
{
  // Technically for mapped nodes should have typedefs for key/mapped type
  using value_type = T;
  using NodePtr = std::unique_ptr<Node<T>>;

  Node<T>* m_parent;
  NodePtr left;
  NodePtr right;
  T value;
  Color color;
  Node () = default;
  Node (const T& val, Node<T>* parent)
    : m_parent (parent)
    , value (val)
  {}

  template <typename A>
  friend std::ostream& operator<< (std::ostream& os, const Node<A>& node);
};

template <typename T>
std::ostream& operator<< (std::ostream& os, const Node<T>& node)
{
  return os << node.value;
}

template <typename Node>
Node* leftmost_child (Node* node)
{
  if (node == nullptr)
  {
    return node;
  }
  while (node->left)
  {
    assert (node);
    node = node->left.get ();
  }
  return node;
}

template <typename Node>
Node* increment (Node* node)
{
  /*
   * Ends on root when parent == nullptr
   */
  /*
   * Keep moving up as long as we are the right child of the parent - this 
   * means we skip over what we have already visited
   * Then move to the parent
   */
  assert (node);
  if (node->right)
  {
    return leftmost_child (node->right.get ());
  }
  // Can't make this a do while don't try ffs
  while (node->m_parent != nullptr && node->m_parent->right.get () == node)
  {
    node = node->m_parent;
  }
  node = node->m_parent;
  return node;
}

/*
 * After lengthy consideration, I have decided to implement rightmost and
 * decrement simply as copies of the functions. I cannot think of a neat other
 * way, closest is parameter (or template parameter?) that is essentially
 * &Node::left then std::invoke (left, node) type but bleh.
 * I am open to reconsideration of this.
 */

template <typename Node>
Node* rightmost_child (Node* node)
{
  if (node == nullptr)
  {
    return node;
  }
  while (node->right)
  {
    assert (node);
    node = node->right.get ();
  }
  return node;
}

template <typename Node>
Node* decrement (Node* node)
{
  assert (node);
  if (node->left)
  {
    return rightmost_child (node->left.get ());
  }
  while (node->m_parent != nullptr && node->m_parent->left.get () == node)
  {
    node = node->m_parent;
  }
  node = node->m_parent;
  return node;
}

/*
 * Experiment first with just T as iterator and yielding node type?
 * Have decided to go for RB tree with map only
 */
// Mutable iterator first
template <typename T>
class TreeIterator
{
  private:
    using NodePtr = std::remove_cv_t<T>*;
    using first_type = const typename std::remove_cv_t<T>::value_type::first_type;
    using second_type = typename std::remove_cv_t<T>::value_type::second_type;
    using pair_type = std::pair<first_type, second_type>;
  public:
    using value_type =
      std::conditional_t<std::is_const_v<T>, const pair_type, pair_type>;
    using difference_type = std::ptrdiff_t;
    using reference = value_type&;
    using pointer = value_type*;
    using iterator_category = std::bidirectional_iterator_tag;
  public:

    reference operator* () const
    {
      return m_node->value;
    }

    auto& operator++ ()
    {
      m_node = increment (m_node);
      return *this;
    }

    auto& operator-- ()
    {
      if (m_node == nullptr) // If at root
      {
        m_node = rightmost_child (m_root);
      }
      else
      {
        m_node = decrement (m_node);
      }
      return *this;
    }

    bool operator== (TreeIterator<T> i) const
    {
      // If this fails we're comparing an iterator from a different tree
      // Don't expect to ever do that?
      assert (m_root == i.m_root);
      return m_node == i.m_node;
    }

    bool operator!= (TreeIterator<T> i) const
    {
      return !(*this == i);
    }

    auto operator++ (int)
    {
      const auto me = *this;
      this->operator++ ();
      return me;
    }

    auto operator-- (int)
    {
      const auto me = *this;
      this->operator-- ();
      return me;
    }

		// Useful for dev
		NodePtr node () const
		{
			return m_node;
		}

    TreeIterator (NodePtr node, NodePtr root)
      : m_node (node)
      , m_root (root)
  {}

  private:
    NodePtr m_node;
    NodePtr m_root;
};

void bisect (
    const std::size_t depth,
    std::vector<std::vector<std::size_t>>& outputs,
    std::size_t start,
    std::size_t end)
{
  if (depth >= outputs.size ())
  {
    return;
  }
  const auto mid = (start + end) / 2;
  outputs.at (depth).push_back (mid);
  bisect (depth + 1, outputs, start, mid);
  bisect (depth + 1, outputs, mid, end);
}

/*
 * This function is pretty grossly inefficient but I'm leaving it like this
 * for now
 */
template <typename T>
std::ostream& print_tree (std::ostream& os, const T& tree)
{
  if (tree.size () == 0)
  {
    return os;
  }
  using Ptr = const std::unique_ptr<Node<typename T::value_type>>*;
  const auto dummy = Ptr {};
  std::vector<std::vector<Ptr>> lines;

  lines.emplace_back ().emplace_back (&tree.m_root);
  lines.reserve (tree.depth ());

  // FIXME: implement own streambuf thing that isn't inefficient as hell?
  std::size_t ele_width = 1;
  for (const auto& e: tree)
  {
    std::stringstream ss;
    ss << e;
    ele_width = std::max (ele_width, ss.str ().size ());
  }
	ele_width += 1; // We append R/B for Red/Black

  const std::size_t space_width = 1;
  // What is portable solution to this - decltype (tree.depth ()) {1} ?
  const auto n = std::pow (2, std::max (1UL, tree.depth ()) - 1);
  const auto elems_width = n * ele_width;
  const auto spaces_width = (n + 1) * space_width;

  const std::size_t max_width = elems_width + spaces_width;
  std::vector<std::vector<std::size_t>> outputs {tree.depth ()};
  bisect (0, outputs, 0, max_width);

  /*
   * Assuming all elements are string width 2 and depth d
   * 2^d elements on the final row each of width 2
   * + 2^d + 1 spaces each of width 2
   * 2^d * 2 + (2^d + 1) * 2 =
   * 4 * 2^d + 2 =
   * 2^(d+2) + 2
   * Means 2^(d+1) + 1 width row
   */

  // This will hopefully be easier once make breadth first iterators?
  for (std::size_t depth = 0; depth < tree.depth (); ++depth)
  {

    /*
     * Print previous line
     */
    const auto& previous_line = lines.back ();

    std::string s (max_width, ' ');
    const auto& offsets = outputs.at (depth);
    for (std::size_t i = 0; i < previous_line.size (); ++i)
    {
      const auto& offset = offsets.at (i);
      const auto& p = previous_line.at (i);
      auto str = std::string (ele_width, '_');
      if (p != dummy)
      {
        std::stringstream ss;
        ss << (*p)->value;
        str = ss.str () + ((*p)->color == Color::BLACK ? "B" : "R");
      }
      s.replace (offset - std::max (1UL, ele_width / 2), 1, str);
    }
    os << s;

    // Messy...
    if (depth + 1 == tree.depth ())
    {
      break;
    }

    os << "\n";

    /*
     * Add next line to queue
     */
    auto& line = lines.emplace_back ();
    for (const auto nodeptr_ptr: previous_line)
    {
      if (nodeptr_ptr == dummy)
      {
        line.push_back (dummy);
        line.push_back (dummy);
      }
      else
      {
        const auto& nodeptr = *nodeptr_ptr;
        line.push_back (nodeptr->left ? &nodeptr->left : dummy);
        line.push_back (nodeptr->right ? &nodeptr->right : dummy);
      }
    }

  }

  return os;
}

template <typename Key, typename T, typename Compare = std::less<Key>>
struct Tree
{
  //using node = Node<T>; // Unsure if I like lowercase for type...; Unsure about this typedef in general

  using key_type = Key;
  using mapped_type = T;
  using value_type = std::pair<const Key, T>;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using key_compare = Compare;
  // TODO: add support for custom comparator; using key_compare = 
  // TODO: add allocator; using allocator_type =
  using reference = value_type&;
  using const_reference = const value_type&;
  // TODO: pointer typedefs with allocator_traits

  using iterator = TreeIterator<Node<value_type>>;
  using const_iterator = TreeIterator<const Node<value_type>>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  std::size_t m_size;
  std::size_t m_depth;
  std::unique_ptr<Node<value_type>> m_root;
  Compare m_compare;

  //std::unique_ptr<Node<value_type>> m_end; // TODO: unsure if this needs to be unique_ptr or just raw
  // m_end.get () == nullptr, unsure if this is fine for all cases

  void insert (const value_type& val)
  {
    m_depth = std::max (m_depth, insert_helper (m_root, val, 1, nullptr));
    ++m_size;
  }

  std::size_t insert_helper (
      std::unique_ptr<Node<value_type>>& node,
      const value_type& val,
      const std::size_t depth,
      Node<value_type>* parent)
  {
    if (!node)
    {
      node = std::make_unique<Node<value_type>> (val, parent);
      return depth;
    }
    // For now restrict to map not multimap - don't have to deal with <= just <
    else if (m_compare (val.first, node->value.first))
    {
      return insert_helper (node->left, val, depth + 1, node.get ());
    }
    else
    {
      return insert_helper (node->right, val, depth + 1, node.get ());
    }
  }

  // Have decided that a tree with one element has a depth 1, not 0
  std::size_t depth () const
  {
    return m_depth;
  }

	bool empty () const
	{
		return m_size == 0;
	}

  std::size_t size () const
  {
    return m_size;
  }

  iterator begin () 
  {
    return iterator (leftmost_child (m_root.get ()), m_root.get ());
  }

  const_iterator begin () const
  {
    return const_iterator (leftmost_child (m_root.get ()), m_root.get ());
  }

  iterator end () 
  {
    return iterator (nullptr, m_root.get ());
  }

  const_iterator end () const
  {
    return const_iterator (nullptr, m_root.get ());
  }

  reverse_iterator rbegin () 
  {
    return reverse_iterator (end ());
  }

  const_reverse_iterator rbegin () const
  {
    return const_reverse_iterator (end ());
  }

  reverse_iterator rend () 
  {
    return reverse_iterator (begin ());
  }

  const_reverse_iterator rend () const
  {
    return const_reverse_iterator (begin ());
  }

  Tree (const Compare& compare = Compare ())
    : m_size (0)
    , m_depth (0)
    , m_root ()
    , m_compare (compare)
  {
  }

  template <typename A, typename B>
  friend std::ostream& operator<< (std::ostream& os, const Tree<A, B>& tree);
};

template <typename Key, typename T>
std::ostream& operator<< (std::ostream& os, const Tree<Key, T>& tree)
{
  return print_tree (os, tree);
}

template <typename N>
std::ptrdiff_t black_nodes (const std::unique_ptr<N>& node)
{
	if (!node)
	{
		return 0;
	}
	const auto left = black_nodes (node->left);
	auto right = black_nodes (node->right);
	if (left == right)
	{
		if (node->color == Color::BLACK)
		{
			return left + 1;
		}
		else
		{
			return left;
		}
	}
	return -1;
}

template <typename N>
std::unique_ptr<N>& owner (const std::unique_ptr<N>& node)
{
	assert (node);
	assert (node->m_parent); // Cannot get parent of root
	const auto& parent = node->m_parent;
	// For now
	assert (parent->left);
	assert (parent->right);
	if (parent->left && parent->left == node)
	{
		return parent->left;
	}
	else if (parent->right && parent->right == node)
	{
		return parent->right;
	}
	assert (false && "No cannot find owner of this pointer");
}

template <typename N>
void rotate_left (std::unique_ptr<N>& node)
{
	std::cout << "At node: " << *node << "\n";
	assert (node);
	assert (node->right);
	auto& owning = owner (node);
	auto right = std::move (node->right);
	right->left = std::move (node);

}

template <typename Tree>
std::pair<bool, const char*> valid_rbtree (const Tree& t)
{
	if (t.empty ())
	{
		return {true, ""};
	}
	if (t.m_root->color != Color::BLACK)
	{
		return {false, "Root must be black"};
	}
	for (auto it = t.begin (); it != t.end (); ++it)
	{
		const auto& node = *it.node ();
		// Red nodes must have black children
		if (node.color == Color::RED)
		{
			assert (node.left);
			assert (node.right);
			if (node.left->color != Color::BLACK)
			{
				return {false, "Left child of a red node must be black"};
			}
			if (node.right->color != Color::BLACK)
			{
				return {false, "Right child of a red node must be black"};
			}
		}
		// Leaf nodes must be black
		if (!node.left && !node.right)
		{
			if (node.color != Color::BLACK)
			{
				return {false, "Leaf nodes must be black"};
			}
		}
	}
	// For any node, the path to any "empty" child node must contain the same
	// number of black nodes
	if (black_nodes (t.m_root) == -1)
	{
		return {false, "Path from root to any child node must contain same "
			"number of black nodes"};
	}
	return {true, ""};
}

auto gapped_tree ()
{
  Tree<int, int> t {};
  // Depth
  t.insert ({23, 0});

  t.insert ({20, 0});
  t.insert ({30, 0});

  t.insert ({15, 0});
  t.insert ({22, 0});
  t.insert ({25, 0});

  t.insert ({12, 0});
  t.insert ({18, 0});
  t.insert ({22, 0});
  t.insert ({28, 0});
  return t;
}

int main ()
{
	{
		auto t = gapped_tree ();
		std::cout << t << "\n";
		auto [valid, msg] = valid_rbtree (t);
		std::cout << "\n";
		if (!valid)
		{
			std::cout << msg << "\n";
		}
		std::cout << "\n";
		//assert (valid);
		rotate_left (t.m_root->left);
		std::cout << t << "\n";
		//auto node = (++t.begin ());
	}

#if 0
  {
    Tree<int, int> t {};
    // Depth
    t.insert ({20, 0});

    t.insert ({15, 0});
    t.insert ({25, 0});

    t.insert ({12, 0});
    t.insert ({18, 0});
    //t.insert ({22, 0});
    t.insert ({28, 0});

    t.insert ({11, 0});
    t.insert ({14, 0});
    t.insert ({16, 0});
    t.insert ({19, 0});
    t.insert ({21, 0});
    t.insert ({23, 0});
    t.insert ({27, 0});
    t.insert ({29, 0});

    t.insert ({10, 0});

    for (auto it = t.begin (); it != t.end (); ++it)
    {
      std::cout << *it << "\n";
    }

    for (const auto& i: t)
    {
      std::cout << i << "\n";
    }

    std::cout << "Tree of depth: " << t.depth () << "\n";

    std::cout << t << "\n";
  }

  {
    Tree<int, int> t {};
    assert (t.begin () == t.end ());
    for (auto& i: t)
    {
      std::cout << i << "\n";
    }
    std::cout << "Empty tree" << "\n";
    std::cout << t << "\n";
  }

  {
    const auto t = gapped_tree ();
    for (const auto& [k, v]: t)
    {
      std::cout << k << ", " << v << "\n";
    }
    //t.begin ()->second = 69;
  }

  {
    Tree<int, int> t {};
    assert (t.begin () == t.end ());
  }

  {
    std::cout << "Should have 69 :" << "\n";
    auto t = gapped_tree ();
    (*++t.begin ()).second = 69;
    std::cout << t << "\n";
  }

  {
    std::cout << "In reverse" << "\n";
    auto t = gapped_tree ();
    for (auto it = t.rbegin (); it != t.rend (); ++it)
    {
      std::cout << *it << "\n";
    }
  }

  std::cout << "\n\n";
#endif

  //std::cout << *deepest_child (t.m_root.get (), &Node<int>::left) << "\n";
  //std::cout << *deepest_child (t.m_root.get (), &Node<int>::right) << "\n";

  /*
  std::cout << "Forward traversal" << "\n";
  for (auto it = t.begin (); it != t.end ();)
  {
    std::cout << *it << "\n";
    it = t.increment (it);
  }
  */

  /*
  std::cout << "Backwards traversal" << "\n";
  for (auto it = t.rbegin (); it != t.rend ();)
  {
    std::cout << *it << "\n";
    it = t.decrement (it);
  }
  */

  //std::cout << *leftmost_child (t.m_root) << "\n";

}

#if 0
template <typename Box, typename Delimiter, typename Transformer>
std::string join (
    Box& container,
    const Delimiter& delimiter,
    Transformer&& f)
{
  // TODO: make more efficient
  auto first = std::begin (container);
  const auto last = std::end (container);
  if (first == last)
  {
    return {};
  }
  std::stringstream ss;
  ss << f (*first++);
  for (; first != last; ++first)
  {
    ss << delimiter << f (*first);
  }
  return ss.str ();
}

template <typename Container, typename Delimiter>
std::string join (
    const Container& container,
    const Delimiter& delimiter)
{
  // TODO: not a specialisation but a different function
  // Default template argument as identity
  return join (container, delimiter, [] (const auto& i) { return i; });
}
#endif
